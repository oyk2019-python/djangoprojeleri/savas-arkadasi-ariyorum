from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import gettext_noop
from django.views.generic import ListView, DetailView, RedirectView, CreateView, UpdateView

from rental.forms import ItemCreateForm, ItemUpdateForm
from rental.models import Items, ItemRental


class ItemListView(ListView):
    model = Items

    def get_queryset(self):
        qs = Q(is_available_for_rental=True)
        if self.request.user.is_authenticated:
            qs |= Q(owner=self.request.user)
        return super().get_queryset().filter(qs)


class ItemDetailView(DetailView):
    model = Items


class ItemCreateView(LoginRequiredMixin, CreateView):
    model = Items
    form_class = ItemCreateForm

    def get_success_url(self):
        return reverse("item_detail", kwargs={"pk": self.object.id})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs


class ItemUpdateView(LoginRequiredMixin, UpdateView):
    model = Items
    form_class = ItemUpdateForm

    def get_success_url(self):
        return reverse("item_detail", kwargs={"pk": self.object.id})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs


class ItemActionView(LoginRequiredMixin, RedirectView):
    action = None
    validate = False
    url = None

    def get_redirect_url(self, *args, **kwargs):
        return self.url or reverse("item_detail", kwargs={'pk': self.kwargs["pk"]})

    def apply_action(self, action):
        if action == gettext_noop("rent"):
            ItemRental.objects.create(hirer=self.request.user, item=self.item)
        elif action == gettext_noop("edit"):
            self.url = reverse("item_edit_form", kwargs={"pk": self.item.id})
        elif action == gettext_noop("accept"):
            rental = ItemRental.objects.get(id=self.kwargs["rental_pk"])
            rental.fulfilled = True
            rental.save(update_fields=["fulfilled"])
        elif action == gettext_noop("switch"):
            self.item.is_available_for_rental = not self.item.is_available_for_rental
            self.item.save(update_fields=["is_available_for_rental"])
        elif action == gettext_noop("remove"):
            self.item.delete()
            self.url = reverse("item_list")
        else:
            pass

    def get(self, request, *args, **kwargs):
        self.item = Items.objects.get(id=self.kwargs["pk"])
        if request.GET.get("valid") == 'false':
            return super().get(request, *args, **kwargs)
        elif self.validate and not request.GET.get("valid") == 'true':
            return render(request, "base/validate.html", {"object": self.item, "action": self.action})
        else:
            self.apply_action(self.action)
            return super().get(request, *args, **kwargs)
