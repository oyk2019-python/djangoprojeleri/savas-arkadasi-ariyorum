from django import forms

from rental.models import Items


class ItemCreateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        obj = super().save(commit=False)
        obj.owner = self.request.user
        obj.save(commit)
        return obj

    class Meta:
        model = Items
        fields = ("name", "type", "image", "description", "is_available_for_rental")


class ItemUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        obj = super().save(commit=False)
        obj.owner = self.request.user
        obj.save(commit)
        return obj

    class Meta:
        model = Items
        fields = ("name", "type", "image", "description")
