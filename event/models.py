from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _


class Event(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=60)
    location = models.CharField(max_length=20)
    date = models.DateField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    description = models.TextField(max_length=300)

    def __str__(self):
        return self.name


class EventComment(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='comments', verbose_name=_("Event"))
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, verbose_name=_("Owner"))
    parent = models.ForeignKey('self', on_delete=models.CASCADE, related_name='replies', blank=True, null=True,
                               verbose_name=_("Parent"))
    comment = models.TextField(max_length=140, verbose_name=_("Comment"))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_("Created"))
    updated = models.DateTimeField(auto_now=True, verbose_name=_("Updated"))
    image = models.ImageField(blank=True, null=True, verbose_name=_("Image"))

    def __str__(self):
        return self.comment


class Participation(models.Model):
    participant = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name=_("Participant"))
    event = models.ForeignKey(Event, on_delete=models.CASCADE, verbose_name=_("Event"))
    team = models.ForeignKey('Team', on_delete=models.SET_NULL, null=True, verbose_name=_("Team"))


class Team(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name=_("Owner"))
    event = models.ForeignKey(Event, on_delete=models.CASCADE, verbose_name=_("Event"))
    name = models.CharField(max_length=32, verbose_name=_("Name"))

    def __str__(self):
        return self.name
