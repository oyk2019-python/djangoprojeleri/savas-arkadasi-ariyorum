from django.contrib import admin
from .models import Event, EventComment, Team, Participation


admin.site.register(Event)
admin.site.register(Team)
admin.site.register(Participation)
admin.site.register(EventComment)
